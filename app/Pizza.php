<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    protected $primaryKey = 'id_pizza';
    protected $table="pizza";
    protected $fillable=["title","price","content","gambar"];
}
