<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('User.user');
});
Route::get('/noodles', function () {
    return view('Admin.noodles');
});
Route::get('/pizza', function () {
    return view('Admin.pizza');
});
