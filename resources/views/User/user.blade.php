<!DOCTYPE html>
<html>
<!--Head-->
@include('User.include.head')
<!--/Head-->
<!--Simple House https://templatemo.com/tm-539-simple-house-->
<body> 
	<div  class="container" >
		<!-- Top box -->
        @include('User.include.top_box')

        <div id="user">

        </div>
		<!--Footer-->
        @include('User.include.footer')
	</div>
	<!--Script-->
    @include('User.include.script')
	
</body>
</html>