<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Noodles extends Model
{
    protected $primaryKey = 'id_noodles';
    protected $table="noodles";
    protected $fillable=["isi","harga","foto","judul"];
}
