<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pizza;
use Illuminate\Support\Facades\Validator;
class PizzaController extends Controller
{
    public function index(){
        $pizza=Pizza::latest()->get();
        return response([
            'success'=>true,
            'message'=>'Daftar Semua Data Berita',
            'data'=>$pizza
        ],200);
    }
    public function store(Request $request){
        $validasi=Validator::make($request->all(),
            [
                'title'=>'required',
                'price'=>'required',
                'content'=>'required',
                'gambar'=>'required'
            ],
            [
                'title.required'=>"Masukan Title Berita! ",
                'price.required'=>"Masukan Price Berita! ",
                'content.required'=>"Masukan Content Berita! ",
                'gambar.required'=>"Masukan Gambar Berita! "
            ]
        );
        if($validasi->fails()){
            return response()->json([
                'success'=>false,
                'message'=>'Silahkan Isi Kolom Yang Kosong',
                'data'=>$validasi->errors()
            ],400);
        }else{
            $pizza=Pizza::create([
                "title"=>$request->input('title'),
                "price"=>$request->input('price'),
                "content"=>$request->input('content'),
                "gambar"=>$request->input('gambar')
            ]);
            if($pizza){
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berita Berhasil Disimpan!',
                ], 200);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'Data Berita Gagal Disimpan',
                ], 400);
            }
        }
    }
    public function show($id){
        $pizza=Pizza::whereid_pizza($id)->first();
        if($pizza){
            return response()->json([
                'success'=>true,
                'message'=>'Detail Data Berita',
                'data'=>$pizza
            ],200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Detail Data Berita Tidak Ditemukan!',
                'data'    => ''
            ], 404);
        }
    }
    public function destroy($id_pizza){
        $pizza=Pizza::findOrFail($id_pizza);
        $pizza->delete();
        if($pizza){
            return response()->json([
                'success' => true,
                'message' => 'Data Berita Berhasil Dihapus!',
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Berhasil Gagal Dihapus!',
            ], 500);
        }
    }
    public function update(Request $request){
        $validasi=Validator::make($request->all(),
            [
                'title'=>'required',
                'price'=>'required',
                'content'=>'required',
                'gambar'=>'required'
            ],
            [
                'title.required'=>"Masukan Title Berita! ",
                'price.required'=>"Masukan Price Berita! ",
                'content.required'=>"Masukan Content Berita! ",
                'gambar.required'=>"Masukan Gambar Berita! "
            ]
        );
        if($validasi->fails()){
            return response()-json([
                'success'=>false,
                'message'=>'Silahkan isi Kolom Yang Kosong',
                'data'=>$validasi->errors()
            ],400);
        }else{
            $pizza=Pizza::whereid_pizza($request->input('id_pizza'))->update([
                "title"=>$request->input('title'),
                "price"=>$request->input('price'),
                "content"=>$request->input('content'),
                "gambar"=>$request->input('gambar')
            ]);
            if($pizza){
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berita Berhasil Diupdate!',
                ], 200);
            }else {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Berita Gagal Diupdate!',
                ], 500);
            }
        }
    }
}
