<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Noodles;
use App\Pizza;
class HomeController extends Controller
{
    public function index1(){
        $noodles=Noodles::latest()->get();
        return response([
            'success'=>true,
            'message'=>'Daftar Semua Data Berita',
            'data'=>$noodles
        ],200);
    }
    public function index2(){
        $pizza=Pizza::latest()->get();
        return response([
            'success'=>true,
            'message'=>'Daftar Semua Data Berita',
            'data'=>$pizza
        ],200);
    }
}
