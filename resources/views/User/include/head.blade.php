<head>
	<meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Simple House Template</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" />    
	<link href="/css/templatemo-style.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/album/">
	<script type="text/javascript" src="js/bootstrap.js"></script>	
	<script type="text/javascript" src="js/jquery.js"></script>
</head>