<!DOCTYPE html>
<html>

<!--Head-->
@include('Admin.include.head')
<body>
  <!-- Sidenav -->
  @include('Admin.include.navbar')
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    @include('Admin.include.top_nav')
    <!-- Header -->
    @include('Admin.include.header')

    <!-- Page content -->
    <div class="container-fluid mt--6" id=pizza>
      
    </div>
    <!-- Footer -->
    @include('Admin.include.footer')
  </div>
  <!-- Argon Scripts -->
  @include('Admin.include.script') 

</body>
</html>
