<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Noodles;
use Illuminate\Support\Facades\Validator;
class NoodlesController extends Controller
{
    public function index(){
        $noodles=Noodles::latest()->get();
        return response([
            'success'=>true,
            'message'=>'Daftar Semua Data Berita',
            'data'=>$noodles
        ],200);
    }
    public function store(Request $request){
        $validasi=Validator::make($request->all(),
            [
                'isi'=>'required',
                'harga'=>'required',
                'foto'=>'required',
                'judul'=>'required'
            ],
            [
                'isi.required'=>"Masukan Isi Berita! ",
                'harga.required'=>"Masukan Harga Berita! ",
                'foto.required'=>"Masukan foto Berita! ",
                'judul.required'=>"Masukan Judul Berita! "
            ]
        );
        if($validasi->fails()){
            return response()->json([
                'success'=>false,
                'message'=>'Silahkan Isi Kolom Yang Kosong',
                'data'=>$validasi->errors()
            ],400);
        }else{
            $noodles=Noodles::create([
                "isi"=>$request->input('isi'),
                "harga"=>$request->input('harga'),
                "foto"=>$request->input('foto'),
                "judul"=>$request->input('judul')
            ]);
            if($noodles){
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berita Berhasil Disimpan!',
                ], 200);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'Data Berita Gagal Disimpan',
                ], 400);
            }
        }
    }
    public function show($id){
        $noodles=Noodles::whereid_noodles($id)->first();
        if($noodles){
            return response()->json([
                'success'=>true,
                'message'=>'Detail Data Berita',
                'data'=>$noodles
            ],200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Detail Data Berita Tidak Ditemukan!',
                'data'    => ''
            ], 404);
        }
    }
    public function destroy($id_noodles){
        $noodles=Noodles::findOrFail($id_noodles);
        $noodles->delete();
        if($noodles){
            return response()->json([
                'success' => true,
                'message' => 'Data Berita Berhasil Dihapus!',
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Berhasil Gagal Dihapus!',
            ], 500);
        }
    }
    public function update(Request $request){
        $validasi=Validator::make($request->all(),
            [
                'isi'=>'required',
                'harga'=>'required',
                'foto'=>'required',
                'judul'=>'required'
            ],
            [
                'isi.required'=>"Masukan Isi Berita! ",
                'harga.required'=>"Masukan Harga Berita! ",
                'foto.required'=>"Masukan foto Berita! ",
                'judul.required'=>"Masukan Judul Berita! "
            ]
        );
        if($validasi->fails()){
            return response()-json([
                'success'=>false,
                'message'=>'Silahkan isi Kolom Yang Kosong',
                'data'=>$validasi->errors()
            ],400);
        }else{
            $noodles=Noodles::whereid_noodles($request->input('id_noodles'))->update([
                "isi"=>$request->input('isi'),
                "harga"=>$request->input('harga'),
                "foto"=>$request->input('foto'),
                "judul"=>$request->input('judul')
            ]);
            if($noodles){
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berita Berhasil Diupdate!',
                ], 200);
            }else {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Berita Gagal Diupdate!',
                ], 500);
            }
        }
    }
}
