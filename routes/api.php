<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//noodles
Route::get('/noodles', 'NoodlesController@index');
Route::post('/noodles/store', 'NoodlesController@store');
Route::get('/noodles/{id?}', 'NoodlesController@show');
Route::post('/noodles/update/{id?}', 'NoodlesController@update');
Route::delete('/noodles/{id?}', 'NoodlesController@destroy');

//pizza
Route::get('/pizza', 'PizzaController@index');
Route::post('/pizza/store', 'PizzaController@store');
Route::get('/pizza/{id?}', 'PizzaController@show');
Route::post('/pizza/update/{id?}', 'PizzaController@update');
Route::delete('/pizza/{id?}', 'PizzaController@destroy');

//Home
Route::get('/home1', 'HomeController@index1');
Route::get('/home2', 'HomeController@index2');