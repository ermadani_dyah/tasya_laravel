require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';

import App from './App.vue';
Vue.use(VueAxios, axios);
//Noodles
import IndexNoodlesComponent from './components/noodles/Index.vue';
import CreateNoodlesComponent from './components/noodles/Tambah.vue';
import EditNoodlesComponent from './components/noodles/Edit.vue';
//pizza
import IndexPizzaComponent from './components/pizza/Index.vue';
import CreatePizzaComponent from './components/pizza/Tambah.vue';
import EditPizzaComponent from './components/pizza/Edit.vue';

//home
import IndexHomeComponent from './components/home/Index.vue';
const routes = [
    //noodles
    {
        name: 'noodles',
        path: '/noodles',
        component: IndexNoodlesComponent
    },
    {
        name: 'tambahNoodles',
        path: '/TambahNoodles',
        component: CreateNoodlesComponent
    },
    {
        name: 'editNoodles',
        path: '/EditNoodles/:id',
        component: EditNoodlesComponent
    },
    //Pizza
    {
        name: 'pizza',
        path: '/pizza',
        component: IndexPizzaComponent
    },
    {
        name: 'tambahPizza',
        path: '/TambahPizza',
        component: CreatePizzaComponent
    },
    {
        name: 'editPizza',
        path: '/EditPizza/:id',
        component: EditPizzaComponent
    },
    //Home
    {
        name: 'user',
        path: '/',
        component: IndexHomeComponent
    },
];

const router = new VueRouter({
    mode: 'history',
    routes: routes
});

const noodles = new Vue(Vue.util.extend({ router }, App)).$mount('#noodles');
const pizza = new Vue(Vue.util.extend({ router }, App)).$mount('#pizza');
const home = new Vue(Vue.util.extend({ router }, App)).$mount('#user');